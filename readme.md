# libre-slider

| autor: Alejandro Henao

Esta libreria de slider tiene como objetivo brindar liberar al desarrollador
de animaciones y eventos pre-establecidos y no reconfigurables, al brindar una serie
de metodos para el funcionamiento basico del slider pero aun asi, exigiendo al desarrollador
escribir algun codigo para lograr lo que este buscando.


## Contenido

- [Caracteristicas](#caracteristicas)
- [Por Venir](#por-venir)
- [Uso](#uso)

# caracteristicas

- clases de slide personalizado
- animaciones personalizadas en elementos especificos definidos
por el desarrollador
- botones principales de navegacion 
- transicion sencilla de fadein para cambio de slide
- navegacion dragueable

# por venir
- zonas clickeable detiene el avance del slider
- puntos de navegacion


# Metodos

## runNext()
renderizar el proximo slider

## runPrev()
renderizar el elider anterior

## draggable()
activar la navegacion dragueable

## buttons(prev,next)
activar y renderizar la navegacion por botones.
los parametros son textos que apareceran en los botones.

para poner estilos a los botones se pueden llamar por su *id*:
next-[el ultimo selector que usaste en la iniciacion de la clase]

# uso

```
// para la animacion y el reinicio de los estilos
//se debe especificar la clase '.active' en el jquery
// para modificar solo el que este activo en el momento

//definir funcion para setear el stilo por defecto 
function resetAnimation(){
	$('.active .fade').css('color','rgb(0,0,0)');
}

//definir funcion con animaciones
function animation(){
	$('.active .fade').animate({color:'rgb(255,255,255)'},1000);
}

// definir un objeto de la clase SliderFade con los selectores css del container y los slide
//en un objeto literial de js y las funciones de animacion y reinicio de estilos

var slider=new SliderFade({container:'sliderContainer',slide:'slide'},animation,resetAnimation)

//como opcional puede activar la navegacion dragueble con el metodo
slider.draggable();

//opcional activar y renderizar sin estilos los botones basicos de navegacion
slider.buttons('prev','next');


//si se quiere que avance automaticamente cada intervalo de tiempo
definir una funcion con el metodo de la clase

function s(){
	slider.runNext();
}

// usar setInterval para correr la funcion definida anteriormente en un intervalo
//en este caso de 4 segundos

setInterval(s,4000);
```
