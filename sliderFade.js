class SliderFade{
	constructor(obj,animation,resetAnimation){
		this.obj=obj;//obj containing classes on slider containder and slides
		
		this.slides=$(`${obj.container} ${obj.slide}`);
		this.length=this.slides.length;
		
		this.currentIndex=0;

		this.animation=animation ? animation : ()=>console.log('no animation function');
		this.resetAnimation=resetAnimation ? resetAnimation : ()=>console.log('no reset animation function');

		//startup
		this.slides.hide();
		this.show(this.getCurrent())
		this.animation();

	}
	draggable(){
		this.slides.draggable({
			axis:'x',
			stop:(e,ui)=>this.dragNav(ui)
		})
	}
	dragNav(ui){
		if (ui.position.left < 0) {
			this.runNext()
		}else{
			this.runPrev()
		}
	}
	loop(){
		if (this.currentIndex==this.length) {
			this.currentIndex=0
		}else if (this.currentIndex==-1) {
			this.currentIndex=this.length-1
		}
	}
	getCurrent(){
		var current =$(`${this.obj.container} ${this.obj.slide}:eq(${this.currentIndex})`)
		current.addClass('active');
		return current
	}
	next(){
		this.currentIndex++;
		this.loop();
	}
	prev(){
		this.currentIndex--;
		this.loop();
	}
	hide(element){
		element.hide();
	}
	show(element){
		element.fadeIn();
	}
	hidePrev(){
		this.slides.css('left',0)
		this.hide(this.getCurrent());
		this.getCurrent().removeClass('active');
	
	}
	runNext(){
		this.resetAnimation();
		this.hidePrev();
		this.next();
		this.show(this.getCurrent());
		this.animation();
	}
	runPrev(){
		this.resetAnimation();
		this.hidePrev();
		this.prev();
		this.show(this.getCurrent());
		this.animation();
	}
	displaySpecific(toShow){
		this.resetAnimation();
		this.hidePrev();
		this.currentIndex=toShow;
		this.show(this.getCurrent());
		this.animation();

	}
	buttons(prev,next){
		let lastSelector=this.obj.container.split(/[.#]+/).slice(-1)[0];
		
		let idPrev=`prev-${lastSelector}`;
		let idNext=`next-${lastSelector}`;
		
		$(`${this.obj.container}`).append(`
			<button id=${idPrev} >${prev}</button>
			<button id=${idNext}>${next}</button>
			
		`)

		$(`#${idPrev}`).on('click',()=>{
			this.resetAnimation();
			this.runPrev();
		})
		$(`#${idNext}`).on('click',()=>{
			this.resetAnimation();
			this.runNext();
		})
	}
	styleDots(dots){
		dots.css({
				background: 'gray',
				width:'20px',
				height: '20px',
				display: 'inline-block',
				marginRight: '15px',
				borderRadius: '50%'
				
			});
	}
	dots(){
		let run= this.displaySpecific;
		let sliderContainer=$(this.obj.container).append('<div class=sliderDotsContainer>	</div>');
		let dotsContainer=$('.sliderDotsContainer')
		let classIdentifier=null;
		for(let i=0; i < this.length; i++){
			dotsContainer.append(`<div class='sliderDot ${i}'></div>`)
		}
		let dots=$('.sliderDotsContainer div')
		this.styleDots(dots)
		dots.on('click',(e)=>{
			this.styleDots(dots)
			e.currentTarget.style.background='black';
			classIdentifier=e.currentTarget.attributes[0].nodeValue.split(' ').slice(-1)[0]
			$(`.sliderDotsContainer .${classIdentifier}`).addClass('dotActive')
			this.displaySpecific(classIdentifier);
		})
	}
}


function resetAnimation(){
	$('.active .fade').css('color','rgb(0,0,0)');
}
function animation(){
	$('.active .fade').animate({color:'rgb(255,255,255)'},1000);
}
var slider=new SliderFade({container:'.sliderContainer',slide:'.slide'},animation,resetAnimation)
slider.buttons('prev','next')
slider.draggable();
slider.dots();

function s(){
	slider.runNext();
	
}
setInterval(s,4000);

